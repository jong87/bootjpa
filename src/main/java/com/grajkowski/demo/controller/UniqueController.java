package com.grajkowski.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.grajkowski.demo.model.Uniques;
import com.grajkowski.demo.dao.UniquesRepo;


@Controller
public class UniqueController {

	@Autowired
	UniquesRepo repo;
	
	@RequestMapping("/")
	public String home() {
		return "home.jsp";
	}
	
	@RequestMapping("/addUniques")
	public String addUniques(Uniques uniques) {
		repo.save(uniques);
		return "home.jsp";
	}
	
	@RequestMapping("/getUniques")
	public ModelAndView getUniques(@RequestParam int uid) {
		ModelAndView mv = new ModelAndView("showUniques.jsp");
		Uniques uniques = repo.findById(uid).orElse(new Uniques());
		
		System.out.println(repo.findByweapon("Bow"));
		
		mv.addObject(uniques);
		return mv;
	}
}
