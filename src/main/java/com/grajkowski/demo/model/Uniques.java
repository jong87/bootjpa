package com.grajkowski.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity

public class Uniques {

	@Id
	private int uid;
	private String aName;
	private String weapon;
	
	public String getaType() {
		return weapon;
	}
	public void setaType(String aType) {
		this.weapon = aType;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getaName() {
		return aName;
	}
	public void setaName(String aName) {
		this.aName = aName;
	}
	@Override
	public String toString() {
		return "Uniques [uid=" + uid + ", aName=" + aName + ", weapon=" + weapon + "]";
	}
	
}
