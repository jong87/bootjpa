package com.grajkowski.demo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.grajkowski.demo.model.Uniques;


public interface UniquesRepo extends CrudRepository<Uniques, Integer> {
	
	List<Uniques> findByweapon(String weapon);
}